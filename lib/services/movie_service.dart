
import 'package:umdb/bloc/movie_provider.dart';
import 'package:umdb/bloc/movies_bloc.dart';
import 'package:umdb/models/movie_model.dart';
import 'package:synchronized/synchronized.dart';



  List<Movie> _saved = new List<Movie> ();
  List<Movie> _history = new List<Movie> ();



// bool isOpenOnce = false;
// var db = new MovieProvider();
// isOpen () async {
//   await db.openMovieDB();
//   isOpenOnce = true;
// }


class MovieService {

  static var _lock = new Lock();
  List<Movie> getHistoryList() {
    return _history;
  }



  bool addMovieToHistory(Movie movie) {
    bool _bool = false;
    for(var i=0; i< _history.length; i++) {
      if(movie.title == _history[i].title) {
        _bool = false;
        return _bool;
      }
    }
    _history.add(movie);
    _bool = true;
    print('Movie Added to History');
    print('Length of History : ${_history.length}');
    return _bool;
  }

  static Future<Map> getAllMoviesFromSqlite(int page) async {
    _lock = new Lock();
    return _lock.synchronized(() async {
      var db = new MovieProvider();
      await db.openMovieDB();
      List<Movie> movies = await db.paginate(page);
      if (movies != null)
        print('Length of SQLite (load) : ${movies.length}');
      await db.closeMovieDB();
      return {
        "currentPage": page,
        "movies": movies
      };
    });
  }
  static Future<bool> saveAllMoviesIntoSqlite(List<Movie> movies) async {

    _lock = new Lock();
    return _lock.synchronized(() async {
    var db = new MovieProvider();
    await db.openMovieDB();
    print('Length to SQLite (save) : ${movies.length}');
    await db.removeAll();
    await db.insertAll(movies);
    await db.closeMovieDB();
    return true;
    });
  }

  static addMovieToSaved(Movie movie) {
    bool _same = false;
    if(_saved != null && _saved.length != 0) {
    _saved.forEach((savedMovie) {
      if (savedMovie.title == movie.title) {
            _same = true;
      }
    });
    }
    if(!_same)
      _saved.add(movie);
    print('saved in Service (add): ${_saved.length}');
  }
  static getSavedFromSQ({int page: 1}) async {
    var response = await getAllMoviesFromSqlite(page);
      _saved.clear();
      _saved.addAll(response['movies']);
      print('saved in Service (get): ${_saved.length}');
      MoviesBloc().clearMovieList();
      await MoviesBloc().addMovieToBloc(_saved);
  }
  static fetchSavedMovies () async {
    print('**********');
    print('Length of Saved : ${_saved.length}');
      await saveAllMoviesIntoSqlite(_saved);
    getSavedFromSQ();
  }
  static removeMovieFromSaved(Movie movie) {
    if (_saved != null && _saved.length != 0) {
      for(var i=0 ; i<_saved.length ; i++) {
        if(_saved[i].title == movie.title) {
          _saved.removeAt(i);
        }
      }
    }
  }
  static List<Movie> getSaved() {
    return _saved;
  }

}




