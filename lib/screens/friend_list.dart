import 'package:flutter/material.dart';
import 'package:umdb/bloc/bloc_provider.dart';
import 'package:umdb/bloc/friends_bloc.dart';
import 'package:umdb/models/friend_model.dart';
import 'package:umdb/services/friend_service.dart';

class FriendList extends StatefulWidget {
  @override
  _FriendListState createState() => _FriendListState();
}

class _FriendListState extends State<FriendList> {
  FriendsBloc friendsBloc;
  final _textController = TextEditingController();
  List<Friend> _friends;
  int _currentPage = 1;
  bool _isLoading = true;
  bool _same;
  ScrollController _listScrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _friends = new List<Friend>();
    _same = false;
    getFromSQ();
    friendsBloc = new FriendsBloc();
    _listScrollController.addListener(() {
      double maxScroll = _listScrollController.position.maxScrollExtent;
      double currentScroll = _listScrollController.position.pixels;

      if (maxScroll - currentScroll <= 200)
        if (!_isLoading)
          getFromSQ(page: _currentPage + 1);
    });
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FriendsBloc>(
      bloc: friendsBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Friend List"),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: _buildFriendList(),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                controller: _textController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter New Friend',
                ),
                onSubmitted: (value) async {
                  // getFromSQ();
                  _same = false;
                  for(var i=0 ; i<_friends.length ; i++) {
                    if(_friends[i].title == value) {
                      _same = true;
                      break;
                    }
                  }
                  if(_same == false) {
                    Friend friend = new Friend(title: value);
                    _friends.add(friend);
                  }
                  await fetchFriends();
                  _textController.clear();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future fetchFriends() async {
    await FriendService.saveAllFriendsIntoSqlite(_friends);
    await getFromSQ();
  }

  getFromSQ({int page: 1, bool refresh: false}) async {
    setState(() => _isLoading = true );
    var response = await FriendService.getAllFriendsFromSqlite(page);
    setState(() {
      _friends.clear();
      print('list that load from friendLIst 0: ${_friends.length}');
      _friends.addAll(response['friends']);
      print('list that load from friendLIst 1: ${_friends.length}');
      _currentPage = response['currentPage'];
      _isLoading = false;
      friendsBloc.addFriendToBloc(_friends);
    });
  }

  Widget listIsEmpty() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.add),
        Text('Add New Friend'),
      ],
    );
  }
  Widget loadingView() {
    return new Center(
      child: new CircularProgressIndicator(),
    );
  }

  Widget _buildFriendList() {
    return StreamBuilder<List<Friend>>(
        stream: friendsBloc.friendsStream,
        builder: (context, snapshot) {
          return _friends.length == 0 && _isLoading
              ? loadingView()
              : _friends.length == 0
              ? listIsEmpty()
              : new RefreshIndicator(
              child: ListView.separated(
                itemCount: friendsBloc.friends.length,
                separatorBuilder: (context, index) => Divider(),
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text('${snapshot.data[index].title}'),
                  );
                },
              ),
              onRefresh:() async{
                await fetchFriends();
              }
          );
        });
  }
}
