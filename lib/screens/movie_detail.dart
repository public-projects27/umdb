import 'package:flutter/material.dart';
import 'package:umdb/models/movie_model.dart';
import 'package:umdb/ui_widgets/movie_detail.dart';


class MovieDetailPage extends StatefulWidget {
  Movie movie;
  MovieDetailPage(this.movie);
  @override
  _MovieDetailPageState createState() => _MovieDetailPageState(movie);
}

class _MovieDetailPageState extends State<MovieDetailPage> {
   Movie movie;
  _MovieDetailPageState(this.movie);
  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(),
        body: SafeArea(
          child: Container(
            // height: 800,
            // width: 500,
            child: MovieDetail(movie)
          ),
          ),

    );
  }
}
