

import 'package:sqflite/sqflite.dart';
import 'package:umdb/bloc/friends_bloc.dart';
import 'package:umdb/models/friend_model.dart';

class FriendProvider extends FriendsBloc {
  String _tableName = 'friends';

  Future<Friend> insert(Friend friend, {conflictAlgorithm: ConflictAlgorithm.ignore}) async {

    await closeFriendDB();
    await openFriendDB();
    await friendDB.insert(_tableName, friend.toMap(), conflictAlgorithm: conflictAlgorithm);
    return friend;
  }

  Future<bool> insertAll(List<Friend> friends) async {
    await Future.wait(friends.map((friend) async {
      if(friend != null)
        await this.insert(friend);
    }
    ));
    return true;
  }
  Future<List<Friend>> paginate(int page, {int limit: 15}) async {
    await closeFriendDB();
    await openFriendDB();
    List<Map> maps = await friendDB.query(_tableName,
        columns: ['Id', 'Title'],
        limit: limit,
        offset: page == 1 ? 0 : ((page -1) * limit)
    );
    List<Friend> friends = [];
    if (maps.length > 0) {
      maps.forEach((friend) {
        if (friend != null) {
          friends.add(Friend.fromJson(friend));
        }
      });
    }
    return friends;
  }

}