
import 'dart:convert';
import 'package:json_annotation/json_annotation.dart';
import 'package:umdb/models/movie_model.dart';
/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'movie_pageination.g.dart';
MoviePageination moviePageinationFromJson(String str) => MoviePageination.fromJson(json.decode(str));

String moviePageinationToJson(MoviePageination data) => json.encode(data.toJson());
/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

@JsonSerializable()
class MoviePageination {
  @JsonKey(name: 'current_page')
  int currentPage;
  @JsonKey(name: 'movies')
  List<Movie> movies;
  @JsonKey(name: 'first_page_url')
  String firstPageUrl;
  @JsonKey(name: 'from')
  int from;
  @JsonKey(name: 'last_page')
  int lastPage;
  @JsonKey(name: 'last_page_url')
  String lastPageUrl;
  @JsonKey(name: 'next_page_url')
  String nextPageUrl;
  @JsonKey(name: 'prev_page_url')
  String prevPageUrl;
  @JsonKey(name: 'to')
  int to;
  @JsonKey(name: 'total')
  int total;


  MoviePageination(
      this.currentPage,
      this.movies,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.prevPageUrl,
      this.to,
      this.total);

  factory MoviePageination.fromJson(Map<String, dynamic> json) => _$MoviePageinationFromJson(json);
  Map<String, dynamic> toJson() => _$MoviePageinationToJson(this);

  @override
  String toString() {
    return 'moviePageination{currentPage: $currentPage, movies: $movies, firstPageUrl: $firstPageUrl, from: $from, lastPage: $lastPage, lastPageUrl: $lastPageUrl, nextPageUrl: $nextPageUrl, prevPageUrl: $prevPageUrl, to: $to, total: $total}';
  }


}
