
import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:umdb/models/friend_model.dart';


/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'friend_pageination.g.dart';
FriendPageination friendPageinationFromJson(String str) => FriendPageination.fromJson(json.decode(str));

String friendPageinationToJson(FriendPageination data) => json.encode(data.toJson());
/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class FriendPageination {
  @JsonKey(name: 'current_page')
  int currentPage;
  @JsonKey(name: 'data')
  List<Friend> friends;
  @JsonKey(name: 'first_page_url')
  String firstPageUrl;
  @JsonKey(name: 'from')
  int from;
  @JsonKey(name: 'last_page')
  int lastPage;
  @JsonKey(name: 'last_page_url')
  String lastPageUrl;
  @JsonKey(name: 'next_page_url')
  String nextPageUrl;
  @JsonKey(name: 'prev_page_url')
  String prevPageUrl;
  @JsonKey(name: 'to')
  int to;
  @JsonKey(name: 'total')
  int total;


  FriendPageination(
      this.currentPage,
      this.friends,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.prevPageUrl,
      this.to,
      this.total);

  factory FriendPageination.fromJson(Map<String, dynamic> json) => _$FriendPageinationFromJson(json);
  Map<String, dynamic> toJson() => _$FriendPageinationToJson(this);

  @override
  String toString() {
    return 'friendPageination{currentPage: $currentPage, friends: $friends, firstPageUrl: $firstPageUrl, from: $from, lastPage: $lastPage, lastPageUrl: $lastPageUrl, nextPageUrl: $nextPageUrl, prevPageUrl: $prevPageUrl, to: $to, total: $total}';
  }


}
