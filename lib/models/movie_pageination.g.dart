// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_pageination.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MoviePageination _$MoviePageinationFromJson(Map<String, dynamic> json) {
  return MoviePageination(
    json['current_page'] as int,
    (json['movies'] as List)
        ?.map(
            (e) => e == null ? null : Movie.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['first_page_url'] as String,
    json['from'] as int,
    json['last_page'] as int,
    json['last_page_url'] as String,
    json['next_page_url'] as String,
    json['prev_page_url'] as String,
    json['to'] as int,
    json['total'] as int,
  );
}

Map<String, dynamic> _$MoviePageinationToJson(MoviePageination instance) =>
    <String, dynamic>{
      'current_page': instance.currentPage,
      'movies': instance.movies,
      'first_page_url': instance.firstPageUrl,
      'from': instance.from,
      'last_page': instance.lastPage,
      'last_page_url': instance.lastPageUrl,
      'next_page_url': instance.nextPageUrl,
      'prev_page_url': instance.prevPageUrl,
      'to': instance.to,
      'total': instance.total,
    };
