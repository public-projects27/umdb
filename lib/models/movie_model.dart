import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'movie_model.g.dart';
Movie movieFromJson(String str) => Movie.fromJson(json.decode(str));

String movieToJson(Movie data) => json.encode(data.toJson());


@JsonSerializable()
class Movie {
  @JsonKey(name: 'Id')
  int id;
  @JsonKey(name: 'Title')
  String title;
  @JsonKey(name: 'imdbRating')
  String imdbRating;
  @JsonKey(name: 'Poster')
  String poster;
  @JsonKey(name: 'Plot')
  String plot;
  @JsonKey(name: 'Saved')
  int saved = 0;
  @JsonKey(name: 'Year')
  String year;
  @JsonKey(name: 'Rated')
  String rated;
  @JsonKey(name: 'Released')
  String released;
  @JsonKey(name: 'Runtime')
  String runtime;
  @JsonKey(name: 'Genre')
  String genre;
  @JsonKey(name: 'Director')
  String director;
  @JsonKey(name: 'Writer')
  String writer;
  @JsonKey(name: 'Actors')
  String actors;
  @JsonKey(name: 'Language')
  String language;
  @JsonKey(name: 'Country')
  String country;
  @JsonKey(name: 'Awards')
  String awards;
  @JsonKey(name: 'Ratings')
  List<Rating> ratings;
  @JsonKey(name: 'Metascore')
  String metascore;
  @JsonKey(name: 'imdbVotes')
  String imdbVotes;
  @JsonKey(name: 'imdbId')
  String imdbId;
  @JsonKey(name: 'Type')
  String type;
  @JsonKey(name: 'DVD')
  String dvd;
  @JsonKey(name: 'BoxOffice')
  String boxOffice;
  @JsonKey(name: 'Production')
  String production;
  @JsonKey(name: 'Website')
  String website;
  @JsonKey(name: 'Response')
  String response;

  Movie({
    this.id,
    this.title,
    this.year,
    this.rated,
    this.released,
    this.runtime,
    this.genre,
    this.director,
    this.writer,
    this.actors,
    this.plot,
    this.language,
    this.country,
    this.awards,
    this.poster,
    this.ratings,
    this.metascore,
    this.imdbRating,
    this.imdbVotes,
    this.imdbId,
    this.type,
    this.dvd,
    this.boxOffice,
    this.production,
    this.website,
    this.response,
    this.saved = 0
  });

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);
  Map<String, dynamic> toJson() => _$MovieToJson(this);

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'Id': id,
      'Title': title,
      'imdbRating': imdbRating,
      'Poster': poster,
      'Plot': plot,
      'Saved': saved,
    };
  }
  @override
  String toString() {
    return 'movies{Id: $id, Title: $title, Poster: $poster, Plot: $plot, Saved: $saved}';
  }

}

class Rating {
  String source;
  String value;

  Rating({
    this.source,
    this.value,
  });

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
        source: json["Source"],
        value: json["Value"],
      );

  Map<String, dynamic> toJson() => {
        "Source": source,
        "Value": value,
      };


  }

