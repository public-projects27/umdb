import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:umdb/bloc/movies_bloc.dart';
import 'package:umdb/screens/friend_list.dart';
import 'package:umdb/screens/movie_detail.dart';
import 'package:umdb/screens/movie_search.dart';
import 'package:umdb/services/movie_service.dart';
// import 'dart:ui' as ui;

import 'bloc/movie_search_bloc.dart';
import 'models/movie_model.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UMDB',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeState();
}

class HomeState extends State<Home> {
  MovieSearchBloc movieSearchBloc;
  Movie movie;
  List<Movie> historyList;
  List<Movie> savedList;
  String _tabController;

  @override
  void initState() {
    super.initState();
    movieSearchBloc = MovieSearchBloc();
    historyList = new List<Movie>();
    savedList = new List<Movie>();
    MovieService.getSavedFromSQ();
    savedList = MovieService.getSaved();
    _tabController = 'search';

  }

  void navigateTo(context, Widget screen) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => screen));
  }




  @override
  Widget build(BuildContext context) {

    Size _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('UMDB'),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            RaisedButton(
              color: Color(0xf0f57505),
              child: ListTile(
                leading: new Icon(Icons.person_outline,
                    color: Colors.white, size: 28),
                title: Text(
                  'Friend List',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18),
                ),
              ),
              onPressed: () {
                navigateTo(context, FriendList());
              },
            ),
            SizedBox(
              height: 10,
            ),
            RaisedButton(
              color: Color(0xf00585f5),
              child: ListTile(
                leading: new Icon(
                  Icons.search,
                  color: Colors.white,
                  size: 28,
                ),
                title: Text(
                  'Movie Search',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18),
                ),
              ),
              onPressed: () {
                navigateTo(context, MovieSearch());
              },
            ),
            SizedBox(
              height: 10,
            ),
            new Container(
                height: _tabController == 'saved' ? 300 : 55,
                width: 480,
                color: Color(0xf0ff0fff),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    new SizedBox(
                      height: 15,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: new GestureDetector(
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new SizedBox(
                              width: 1,
                            ),
                            new Icon(
                              Icons.bookmark_border,
                              color: Colors.black54,
                              size: 28,
                            ),
                            new SizedBox(
                              width: 0.5,
                            ),
                            Text(
                              'Saved Movies',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            ),
                            new SizedBox(
                              width: 80,
                            ),
                            GestureDetector(
                              child: new Icon(
                                Icons.autorenew,
                                color: Colors.black,
                                size: 28,
                              ),
                              onTap: () async {
                                setState(() {
                                 savedList = new List<Movie>();
                                 MovieService.getSavedFromSQ();
                                 savedList = MovieService.getSaved();
                                });
                              },
                            ),
                            new SizedBox(
                              width: 2,
                            ),
                          ],
                        ),
                        onTap: () {
                          setState(() {
                            if (_tabController == 'search')
                              _tabController = 'saved';
                            else
                              _tabController = 'search';
                          });
                        },
                      ),
                    ),
                    new SizedBox(
                      height: 10,
                    ),
                    _tabController == 'saved'
                        ? new Container(
                            alignment: Alignment.centerLeft,
                            height: 240,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: savedList != null
                                  ? searchHistory(context, savedList)
                                  : Container(
                                      width: _screenSize.width,
                                      alignment: Alignment.center,
                                      child: new Text('Saved Movies is Empty!'),
                                    ),
                            ))
                        : new Container()
                  ],
                )),
            SizedBox(
              height: 10,
            ),
            new Container(
                height: _tabController == 'search' ? 300 : 55,
                width: 480,
                color: Color(0xf09fff9f),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    new SizedBox(
                      height: 15,
                    ),
                    new Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: new GestureDetector(
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new SizedBox(
                              width: 1,
                            ),
                            new Icon(
                              Icons.history,
                              color: Colors.black54,
                              size: 28,
                            ),
                            new SizedBox(
                              width: 0.5,
                            ),
                            Text(
                              'Search History',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            ),
                            new SizedBox(
                              width: 80,
                            ),
                            GestureDetector(
                              child: new Icon(
                                Icons.autorenew,
                                color: Colors.black,
                                size: 28,
                              ),
                              onTap: () {
                                setState(() {
                                  historyList = new List<Movie>();
                                  historyList = MovieService().getHistoryList();
                                });
                              },
                            ),
                            new SizedBox(
                              width: 2,
                            ),
                          ],
                        ),
                        onTap: () {
                          setState(() {
                            if (_tabController == 'search')
                              _tabController = 'saved';
                            else
                              _tabController = 'search';
                          });
                        },
                      ),
                    ),
                    new SizedBox(
                      height: 10,
                    ),
                    _tabController == 'search'
                        ? new Container(
                            alignment: Alignment.centerLeft,
                            height: 240,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: historyList.length != 0
                                  ? searchHistory(context, historyList)
                                  : Container(
                                      width: _screenSize.width,
                                      alignment: Alignment.center,
                                      child:
                                          new Text('Search History is Empty!'),
                                    ),
                            ))
                        : new Container()
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget searchHistory(BuildContext context, List<Movie> movies) {
    return Container(
        width: movies.length * 153.0,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: movies.length,
            itemBuilder: (context, index) {
              return movieShow(context, movies[index]);
            }));
  }

  Widget movieShow(BuildContext context, Movie movie) {
    return new Stack(children: [
      new GestureDetector(
        child:   new Container(
          margin: const EdgeInsets.all(6.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Container(
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.circular(10.0),
                    image: new DecorationImage(
                        image: new NetworkImage(movie.poster),
                        fit: BoxFit.fitHeight),
                    boxShadow: [
                      new BoxShadow(
                          color: Colors.black,
                          blurRadius: 20.0,
                          offset: new Offset(0.0, 10.0))
                    ]),
                child: new Container(
                  width: 140.0,
                  height: 180.0,
                ),
              ),
              new Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  width: 140,
                  height: 40,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.symmetric(vertical: 2.0),
                  child: new SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: new Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(boxShadow: [
                        new BoxShadow(
                            color: Colors.white,
                            spreadRadius: 5.0,
                            blurRadius: 25.0,
                            offset: new Offset(0.0, 4.0))
                      ]),
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text(
                            '${movie.imdbRating} | 10',
                            style: new TextStyle(
                                color: Colors.black,
                                fontSize: 14.0,
                                fontFamily: 'Arvo'),
                          ),
                          new Text(
                            movie.title,
                            style: new TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontFamily: 'Arvo'),
                            // )
                          ),
                        ],
                      ),
                    ),
                  )),
            ],
          ),
        ),
        onTap: () {
          navigateTo(context, MovieDetailPage(movie));
        },
      )

    ]
        // ),
        );
  }
}


